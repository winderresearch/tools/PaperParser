"""Test abstract parser module."""
from paperparser.abstract_parser import abstract


def test_parse_arxiv_abstract() -> None:
    """Can output arxiv abstract."""
    url = "https://arxiv.org/abs/1812.02900"
    res = abstract(strategy="arxiv", url=url)
    assert res is not None
    assert isinstance(res, str)
    assert "Abstract: Many practical applications of reinforcement" in res


def test_parse_nips_abstract() -> None:
    """Can output nips abstract."""
    url = (
        "http://papers.nips.cc/paper/"
        "8296-multimodal-model-agnostic-meta-learning-via-task-aware-modulation"
    )
    res = abstract(strategy="nips", url=url)
    assert res is not None
    assert isinstance(res, str)
    assert "Model-agnostic meta-learners aim to acquire meta-learned parameters" in res


def test_acm_abstract() -> None:
    """Parse ACM."""
    url = "https://dl.acm.org/doi/10.1016/j.engappai.2004.08.018"
    bib = abstract("acm", url)
    assert isinstance(bib, str)
    assert "Reinforcement learning (RL) has received some attention in recent " in bib


def test_ieee_abstract() -> None:
    """Parse ieee."""
    url = "https://ieeexplore.ieee.org/document/6502719"
    bib = abstract("ieee", url)
    assert isinstance(bib, str)
    assert "Population is steadily increasing worldwide" in bib


def test_sciencedirect() -> None:
    """Can parse sciencedirect paper."""
    url = (
        "https://www.sciencedirect.com/science/article/pii/S0378778816300305?via%3Dihub"
    )
    text = abstract("sciencedirect", url)
    assert isinstance(text, str)
    assert "In a future Smart Grid context, increasing" in text


def test_wiley() -> None:
    """Can parse wiley paper."""
    url = "https://onlinelibrary.wiley.com/doi/abs/10.1002/cpe.2864"
    text = abstract("wiley", url)
    assert isinstance(text, str)
    assert "Public Infrastructure as a Service" in text


def test_semanticscholar() -> None:
    """Can parse semanticsholar paper."""
    url = "https://www.semanticscholar.org/paper/\
        A-Learning-Based-Approach-for-the-Programming-of-Wang-Liang/\
            0345815682cc1de3ef5ad57d038694071b5b488e"
    text = abstract("semanticscholar", url)
    assert isinstance(text, str)
    assert "Given a scene layout like a room or a courtyard composed of objects" in text
