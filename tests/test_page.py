"""Test page module."""
from paperparser import page


def test_dict() -> None:
    """Can output as dict."""
    url = "https://arxiv.org/abs/1812.02900"
    p = page.BibTeXPage(url=url, strategy="arxiv")
    res = p.as_dict()
    assert res is not None
    assert isinstance(res, dict)
    assert res["title"] == "Off-Policy Deep Reinforcement Learning without Exploration"


def test_abstract() -> None:
    """Can output abstract."""
    url = "https://arxiv.org/abs/1812.02900"
    p = page.BibTeXPage(url=url, strategy="arxiv")
    res = p.abstract()
    assert res is not None
    assert isinstance(res, str)
    assert "Abstract: Many practical applications of reinforcement" in res
